# ElixirApi

Simple prototype Elixir HTTP API with JSON :)

You know, just for fun and giggles.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `elixir_api` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:elixir_api, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/elixir_api](https://hexdocs.pm/elixir_api).

## Running

Clone and install dependencies with:

```sh
mix do deps.get, deps.compile, compile
```

Run the code
```sh
mix run --no-halt
```

Test :)

```sh
$ curl http://localhost:4000/
{"text":"Hello World!","response_type":"in_channel"}
```

## Author

Jaakko Leskinen <<jaakko.leskinen@gmail.com>>
