defmodule ElixirApi.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  alias ElixirApi.Endpoint

  def start(_type, _args) do
    # List all child processes to be supervised
    # Add our API endpoint
    children = [
      ElixirApi.Endpoint
      # Starts a worker by calling: ElixirApi.Worker.start_link(arg)
      # {ElixirApi.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ElixirApi.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
